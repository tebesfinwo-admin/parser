# README #

### What is this repository for? ###

A Simple Parser for a simple programming language. 

### How do I get set up? ###

To Compile the source code :
```
$ javac src/*
```

To run 
```
$ java MicroPL86 <fileName>
```

### Who do I talk to? ###

[@tebesfinwo-admin](https://bitbucket.org/tebesfinwo-admin)