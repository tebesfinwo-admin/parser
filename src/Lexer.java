import java.io.*;

/**
 * Created by tebesfinwo on 11/23/14.
 */
public class Lexer {

    InputStream is;
    char currChar;
    Token currentToken = null;
    int line = 1, col = 1;

    static final int
        EOI = -2,
        ERROR = -1,
        START = 0,
        ID = 1,
        GT_OR_GE = 2,
        LT_OR_LE = 3,
        COMMENT = 4,
        ASSIGN_OR_EQUAL = 5,
        INT_LITERAL = 6,
        NOT_EQUAL_OR_ID = 7;

    public Lexer(File file) throws IOException {
        this.is = new FileInputStream(file);
        advance();
        next();
    }

    public Lexer(String s) throws IOException {
        this.is = new ByteArrayInputStream(s.getBytes("UTF-8"));
        advance();
    }

    public Token token(){
        return this.currentToken;
    }

    void advance() throws IOException {
        int i = is.read();
        this.currChar = (i < 0) ? '\0' : (char)i;
        if (this.currChar == '\n') {
            this.line++;
            this.col = 0;
        }
        this.col++;
    }

    public Token next() throws IOException {

        this.currentToken = null;
        String lexeme = null;
        int state = START;


        while (this.currentToken == null) {
            boolean shouldAdvance = true;
            switch (state) {

                case START :
                    lexeme = "";
                    if (Character.isWhitespace(this.currChar))
                        state = START;
                    else if (Character.isDigit(this.currChar))
                        state = INT_LITERAL;
                    else if (this.currChar == '#')
                        state = COMMENT;
                    else if (Character.isLetter(this.currChar))
                        state = ID;
                    else if (this.currChar == ',')
                        this.currentToken = new Token(Token.COMMA);
                    else if (this.currChar == ';')
                        this.currentToken = new Token(Token.SEMICOLON);
                    else if (this.currChar == '=')
                        state = ASSIGN_OR_EQUAL;
                    else if (this.currChar == '!')
                        state = NOT_EQUAL_OR_ID;
                    else if (this.currChar == '+')
                        this.currentToken = new Token(Token.ADD_OP);
                    else if (this.currChar == '-')
                        this.currentToken = new Token(Token.MINUS_OP);
                    else if (this.currChar == '%')
                        this.currentToken = new Token(Token.MOD_OP);
                    else if (this.currChar == '/')
                        this.currentToken = new Token(Token.DIV_OP);
                    else if (this.currChar == '*')
                        this.currentToken = new Token(Token.MUL_OP);
                    else if (this.currChar == '>')
                        state = GT_OR_GE;
                    else if (this.currChar == '<')
                        state = LT_OR_LE;
                    else if (this.currChar == '{')
                        this.currentToken = new Token(Token.LBRACE);
                    else if (this.currChar == '}')
                        this.currentToken = new Token(Token.RBRACE);
                    else if (this.currChar == '(')
                        this.currentToken = new Token(Token.LPAREN);
                    else if (this.currChar == ')')
                        this.currentToken = new Token(Token.RPAREN);
                    else if (this.currChar == '\0')
                        state = EOI;
                    else
                        state = ERROR;
                    break ;


                case COMMENT :
                    if ( this.currChar == '\n') {
                        state = START;
                    } else {
                        state = COMMENT;
                    }
                    break;

                case INT_LITERAL :
                    if ( ! Character.isDigit(this.currChar) ){
                        this.currentToken = new Token(Token.INT_LITERAL, lexeme);
                        shouldAdvance = false;
                    } else {
                        state = INT_LITERAL;
                    }
                    break;

                case ASSIGN_OR_EQUAL :
                    if (this.currChar == '=')
                        this.currentToken = new Token(Token.EQUAL_OP);
                    else
                        this.currentToken = new Token(Token.ASSIGN_SYMBOL);
                    break;

                case ID :
                    if ( Character.isLetterOrDigit(this.currChar) )
                        state = ID;
                    else {
                        if ( Token.isKeyword(lexeme) )
                            this.currentToken = Token.keywordMap.get(lexeme);
                        else
                            this.currentToken = new Token(Token.ID, lexeme);
                        shouldAdvance = false;
                    }
                    break;

                case NOT_EQUAL_OR_ID :
                    if ( this.currChar == '=' ) {
                        this.currentToken = new Token(Token.NOT_EQUAL_OP);
                        shouldAdvance = false;
                    } else if (Character.isDigit(this.currChar))
                        state = ID;
                    else
                        shouldAdvance = false;
                    break;

                case GT_OR_GE :
                    if ( this.currChar == '=' )
                        this.currentToken = new Token(Token.GE);
                    else
                        this.currentToken = new Token(Token.GT);
                    shouldAdvance = false;
                    break;

                case LT_OR_LE :
                    if ( this.currChar == '=' )
                        this.currentToken = new Token(Token.LE);
                    else
                        this.currentToken = new Token(Token.LT);
                    shouldAdvance = false;
                    break;

                case ERROR :
                    System.err.println("Unexpected character " + this.currChar + " on line " + line + " at column  " + col);
                    state = START;
                    break;

                case EOI :
                    this.currentToken = new Token(Token.EOI);
                    break;

                default:
                    System.err.println("Unknown state " + state);
                    System.exit(0);
            }

            lexeme += this.currChar;
            if (shouldAdvance && state != ERROR) advance();
        }

        //System.out.println(lexeme);
        return this.currentToken;
    }




}
