import java.io.File;
import java.io.IOException;

public class MicroPL86 {

    static String fileName = null;

    public static void main(String[] args) {

        processCommandLine(args);

        if (fileName == null){
            System.out.println("Usage: MicroPL86 <filename>");
            System.exit(2);
        }

        try {
            new Parser(new Lexer(new File(fileName))).parse();
            System.out.println("*** Success ***");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (ParserException e) {
            System.err.println(e.getMessage());
        }

    }

    static void processCommandLine(String[] args){
        for(String arg : args){
            if (arg.startsWith("-")){

            }   else {
                fileName = arg;
            }
        }
    }
}
