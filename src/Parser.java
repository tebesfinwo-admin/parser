import java.io.IOException;

/**
 * Created by tebesfinwo on 11/24/14.
 *
 */
public class Parser {

    Lexer lexer;

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }

    void parse() throws IOException, ParserException {
        this.program();
        this.match(Token.EOI);
    }

    private void program() throws IOException, ParserException {
        //System.out.println("Program");
        this.match(Token.PROGRAM);
        while (this.optMatch(Token.INT))
            this.declaration();
        while (!this.optMatch(Token.END))
            this.statement();
    }

    private void declaration() throws IOException, ParserException {
        //System.out.println("declaration");
        this.match(Token.ID);
        while(this.optMatch(Token.COMMA))
            this.match(Token.ID);
        this.match(Token.SEMICOLON);
    }

    private void statement() throws IOException, ParserException {
        //System.out.println("statement");
        if ( this.optMatch(Token.ID) )
            this.assignmentStatement();
        else if (this.optMatch(Token.IF))
            this.ifStatement();
        else if (this.optMatch(Token.WHILE))
            this.whileStatement();
        else if (this.optMatch(Token.FOR))
            this.forStatement();
        else if (this.optMatch(Token.PRINT))
            this.printStatement();
        else if (this.optMatch(Token.READ))
            this.readStatement();
        else if (this.optMatch(Token.LBRACE))
            this.compoundStatement();
        else
            throw new ParserException(lexer, "Expecting statement, found " + lexer.token());
    }

    private void assignmentStatement() throws IOException, ParserException {
        //System.out.println("assignment");
        this.match(Token.ASSIGN_SYMBOL);
        this.expression();
        this.match(Token.SEMICOLON);
    }

    private void ifStatement() throws IOException, ParserException {
        //System.out.println("if");
        this.match(Token.LPAREN);
        this.expression();
        this.relop();
        this.expression();
        this.match(Token.RPAREN);
        this.statement();
        if ( this.optMatch(Token.ELSE) ) {
            this.statement();
        }
    }

    private void whileStatement() throws IOException, ParserException {
        //System.out.println("while");
        this.match(Token.LPAREN);
        this.expression();
        this.relop();
        this.expression();
        this.match(Token.RPAREN);
        this.statement();
    }

    private void forStatement() throws IOException, ParserException {
        //System.out.println("for");
        this.match(Token.LPAREN);
        this.match(Token.ID);
        this.match(Token.ASSIGN_SYMBOL);
        this.expression();
        this.match(Token.COMMA);
        this.expression();
        this.match(Token.RPAREN);
        this.statement();
    }

    private void printStatement() throws IOException, ParserException {
        //System.out.println("print");
        this.expression();
        this.match(Token.SEMICOLON);
    }

    private void readStatement() throws IOException, ParserException {
        //System.out.println("read");
        this.match(Token.ID);
        this.match(Token.SEMICOLON);
//        if ( this.optMatch(Token.ID) )
//            return;
    }

    private void compoundStatement() throws IOException, ParserException {
        //System.out.println("compound");
        //this.match(Token.LPAREN);
        while(!this.optMatch(Token.RBRACE))
            this.statement();
    }

    private void expression() throws IOException, ParserException {
        //System.out.println("expression");
        this.term();
        while ( this.optMatch(Token.ADD_OP) || this.optMatch(Token.MINUS_OP) )
            term();
    }

    private void term() throws IOException, ParserException {
        //System.out.println("term");
        this.factor();
        while ( this.optMatch(Token.MUL_OP) || this.optMatch(Token.DIV_OP) || this.optMatch(Token.MOD_OP) )
            this.factor();
    }

    private void factor() throws IOException, ParserException {
        //System.out.println("factor");
        if (this.optMatch(Token.ID))
            return;
        else if (this.optMatch(Token.INT_LITERAL))
            return;
        else if (this.optMatch(Token.LPAREN)){
            this.expression();
            this.match(Token.RPAREN);
        } else {
            throw new ParserException(this.lexer, "Expecting factor, found " + this.lexer.token());
        }
    }

    private void relop() throws ParserException, IOException {
        if (! this.lexer.token().isRelop())
            throw new ParserException(this.lexer, "Expecting relational, found " + this.lexer.token());
        this.lexer.next();
    }

    private boolean optMatch(int tokenType) throws IOException {
        if (lexer.token().type == tokenType){
            lexer.next();
            return true;
        }
        return false;
    }

    private void match(int tokenType) throws ParserException, IOException {
        if (! this.optMatch(tokenType))
            throw new ParserException(this.lexer, "Expecting '" + new Token(tokenType) + "' , found " + lexer.token());
    }

}
