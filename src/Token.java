import java.util.Map;
import java.util.TreeMap;

/**
 * Created by tebesfinwo on 11/23/14.
 */
public class Token {

    int type;
    String lexeme;

    static final Map<Integer, String> tokenMap = new TreeMap<Integer, String>();

    static final Map<String, Token> keywordMap = new TreeMap<String, Token>();

    static final int
        EOI = 0,
        ID = 1,
        INT_LITERAL = 2,
        EQUAL_OP = 3,
        NOT_EQUAL_OP = 4,
        LT = 5,
        LE = 6,
        GT = 7,
        GE = 8,
        ADD_OP = 9,
        MINUS_OP = 10,
        MUL_OP = 11,
        DIV_OP = 12,
        MOD_OP = 13,
        SEMICOLON = 14,
        COMMA = 15,
        LBRACE = 16,
        RBRACE = 17,
        LPAREN = 18,
        RPAREN = 19,
        PROGRAM = 20,
        INT = 21,
        IF = 22,
        WHILE = 23,
        FOR = 24,
        PRINT = 25,
        READ = 26,
        END = 27,
        ASSIGN_SYMBOL = 28,
        ELSE = 29;


    static {
        tokenMap.put(EOI, "EOI");
        tokenMap.put(ID, "ID");
        tokenMap.put(INT_LITERAL, "INT_LITERAL");
        tokenMap.put(IF, "if");
        tokenMap.put(ELSE, "else");
        tokenMap.put(EQUAL_OP, "EQUAL_OP");
        tokenMap.put(NOT_EQUAL_OP, "NOT_EQUAL_OP");
        tokenMap.put(LT, "LT");
        tokenMap.put(LE, "LE");
        tokenMap.put(GT, "GT");
        tokenMap.put(GE, "GE");
        tokenMap.put(ADD_OP, "ADD_OP");
        tokenMap.put(MINUS_OP, "MINUS_OP");
        tokenMap.put(MUL_OP, "MUL_OP");
        tokenMap.put(DIV_OP, "DIV_OP");
        tokenMap.put(MOD_OP, "MOD_OP");
        tokenMap.put(SEMICOLON, "SEMICOLON");
        tokenMap.put(COMMA, "COMMA");
        tokenMap.put(LBRACE, "LBRACE");
        tokenMap.put(RBRACE, "RBRACE");
        tokenMap.put(LPAREN, "LPAREN");
        tokenMap.put(RPAREN, "RPAREN");
        tokenMap.put(PROGRAM, "program");
        tokenMap.put(INT, "int");
        tokenMap.put(WHILE, "while");
        tokenMap.put(FOR, "for");
        tokenMap.put(PRINT, "print");
        tokenMap.put(READ, "read");
        tokenMap.put(END, "end");
        tokenMap.put(ASSIGN_SYMBOL, "ASSIGN_SYMBOL");
    }

    static {
        keywordMap.put("program", new Token(PROGRAM));
        keywordMap.put("int", new Token(INT));
        keywordMap.put("if", new Token(IF));
        keywordMap.put("else", new Token(ELSE));
        keywordMap.put("while", new Token(WHILE));
        keywordMap.put("for", new Token(FOR));
        keywordMap.put("print", new Token(PRINT));
        keywordMap.put("read", new Token(READ));
        keywordMap.put("end", new Token(END));
    }


    public Token(int type, String lexeme) {
        this.type = type;
        this.lexeme = lexeme;
    }

    public Token(int type) {
        this(type, null);
    }

    public static boolean isKeyword(String lexeme){
        return keywordMap.get(lexeme) != null;
    }

    public boolean isRelop(){
        return (
                EQUAL_OP == this.type ||
                NOT_EQUAL_OP == this.type ||
                LT == this.type ||
                LE == this.type ||
                GT == this.type ||
                GE == this.type
        );
    }

    @Override
    public String toString(){
        return tokenMap.get(this.type) + (lexeme == null ? "" : " (" + lexeme + ")");
    }
}
